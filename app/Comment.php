<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //why is this required when i have $gaurded in my model.php file??
    //protected $fillable = ['body'];

    public function post()

    {

        return $this->belongsTo(Post::class);
        
    }

    public function user()

    {

        return $this->belongsTo(User::class);
        
    }
}
